/// Copyright (c) 2023, Alex Hogen <code.ahogen@outlook.com>
/// SPDX-License-Identifier: MIT
extern crate md5;
extern crate serde;

#[derive(Debug, Clone, PartialEq, serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "camelCase")]
pub enum Severity {
    Info,
    Minor,
    Major,
    Critical,
    Blocker,
}

/// Example GitLab Code Quality JSON object:
///
/// ```json
/// [
///   {
///     "description": "'unused' is assigned a value but never used.",
///     "check_name": "no-unused-vars",
///     "fingerprint": "7815696ecbf1c96e6894b779456d330e",
///     "severity": "minor",
///     "location": {
///       "path": "lib/index.js",
///       "lines": {
///         "begin": 42
///       }
///     }
///   }
/// ]
/// ```
///
/// Refer to:
///   - https://docs.gitlab.com/ee/ci/testing/code_quality.html#implement-a-custom-tool
///   - https://github.com/codeclimate/platform/blob/master/spec/analyzers/SPEC.md#data-types
#[derive(Debug, Clone, serde::Serialize, serde::Deserialize)]
pub struct Issue<'a> {
    /// `type` must always be "issue".
    #[serde(rename(serialize = "type"))]
    issue_type: &'a str,
    /// A unique name representing the static analysis check that emitted this
    /// issue.
    pub check_name: String,
    /// A string explaining the issue that was detected.
    pub description: String,
    /// A unique, deterministic identifier for the specific issue being
    /// reported to allow a user to exclude it from future analyses.
    pub fingerprint: String,
    /// A Severity describing the potential impact of the issue found.
    pub severity: Severity,
    /// A Location object representing the place in the source code where the
    /// issue was discovered.
    pub location: Location,
}

/// GitLab Code Quality 'location' object.
#[derive(Debug, Default, Clone, PartialEq, serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Location {
    pub path: String,
    pub line: i64,
}

impl<'a> Issue<'a> {
    /// Initialize a new issue struct.
    ///
    /// The structure's `type` member is hidden because it's always supposed to
    /// be set to the string `"issue"`.
    /// The structure's `fingerprint` member is not an argument of this method
    /// because after all other fields have been set in the structure,
    /// `self.fingerprint()` is called before returning.
    pub fn new(
        check_name: String,
        description: String,
        severity: Severity,
        location: Location,
    ) -> Self {
        let mut s = Issue {
            issue_type: "issue",
            check_name,
            description,
            fingerprint: "".to_owned(),
            severity,
            location,
        };

        s.fingerprint();

        s
    }

    /// Update fingerprint field. Per Appendix B of SARIF v2.1.0, a fingerprint
    /// should be computed from
    ///   - file path
    ///   - checker rule
    ///   - ~~tool name~~
    ///
    /// However, note that currently "tool name" is not being extracted from
    /// a SARIF report, so it's crossed off the above list for now.
    ///
    /// TODO: Add tool name to hash inputs.
    pub fn fingerprint(&mut self) {
        let in_str = self.location.path.clone() + "-" + &self.check_name;
        let digest = md5::compute(in_str);
        self.fingerprint = format!("{:x}", digest);
    }
}
