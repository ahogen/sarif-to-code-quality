/// Convert SARIF json to Code Quality json files.
///
///
/// Copyright (c) 2023, Alex Hogen <code.ahogen@outlook.com>
/// SPDX-License-Identifier: MIT
use clap::Parser;
use simplelog::*;
use std;
use std::io::{BufReader, Read};
use std::{error::Error, fs, vec::Vec};

use serde_sarif::sarif;

mod codequality;

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    /// Input (SARIF json) file
    #[clap(short, long)]
    input_file: String,

    /// Output (Code Quality json) file
    #[clap(short, long)]
    output_file: String,
}

fn main() {
    // Get CLI arguments
    let args = Args::parse();

    // Setup logger
    CombinedLogger::init(vec![TermLogger::new(
        LevelFilter::Info,
        Config::default(),
        TerminalMode::Mixed,
        ColorChoice::Auto,
    )])
    .unwrap();

    // Catch and log errors, return non-zero.
    if let Err(e) = process(args) {
        error!("{}", e);
        std::process::exit(1);
    }
}

/// Process SARIF input, generate Code Quality output
fn process(args: Args) -> Result<(), Box<dyn Error>> {
    info!("Reading input file '{}'", args.input_file);
    let read = Box::new(fs::File::open(args.input_file)?) as Box<dyn Read>;
    let reader = BufReader::new(read);

    let sarif_data: sarif::Sarif = serde_json::from_reader(reader)?;

    let mut code_climate_data = Vec::new();

    for (run_idx, run) in sarif_data.runs.iter().enumerate() {
        for (result_idx, result) in run
            .results
            .as_ref()
            .ok_or(format!(
                "SARIF `runs[{}]` doesn't have'results' array",
                run_idx
            ))?
            .iter()
            .enumerate()
        {
            // TODO: Support message string lookup.
            // See SARIF v2.1.0 section 3.11.7 "Message string lookup"
            let description: String = result.message.text.clone().ok_or(format!(
                "SARIF missing field 'runs[{}].results[{}].message.text`",
                run_idx, result_idx
            ))?;
            let severity = from_sarif_result(
                result
                    .level
                    .clone()
                    .unwrap_or(serde_json::Value::String("warning".to_string())),
            )?;

            // IDEA: prefix the tool name? e.g. `cppcheck/arrayIndexOutOfBounds`
            let check_name: String = result.rule_id.clone().ok_or(format!(
                "SARIF missing field 'runs[{}].results[{}].ruleID`",
                run_idx, result_idx
            ))?;

            // Extract from the SARIF report the path of the file where the
            // code-quality issue originated.
            let locations = result.locations.as_ref().ok_or(format!(
                "SARIF missing field `runs[{}].results[{}].locations[]`",
                run_idx, result_idx
            ))?;
            let physical_location0 = locations[0].physical_location.as_ref().ok_or(format!(
                "SARIF missing field `runs[{}].results[{}].locations[0].physicalLocation`",
                run_idx, result_idx
            ))?;

            let artifact_location0 = physical_location0
                .artifact_location
                .as_ref()
                .ok_or(format!(
                    "SARIF missing field `runs[{}].results[{}].locations[0].physicalLocation.artifactLocation`",
                    run_idx, result_idx
                ))?;

            let mut file_path: String = artifact_location0
                .uri_base_id
                .clone()
                .unwrap_or("".to_string());

            file_path = file_path
                + "/"
                + artifact_location0
                    .uri
                    .as_ref()
                    .ok_or(format!(
                        "SARIF missing field `runs[{}].results[{}].locations[0].physicalLocation.artifactLocation.uri`",
                        run_idx, result_idx
                    ))?;
            file_path = file_path.replace("%SRCROOT%", ".");

            let file_lineno = physical_location0
                .region
                .as_ref()
                .ok_or(format!(
                    "SARIF missing field `runs[{}].results[{}].locations[0].physicalLocation.region`",
                    run_idx, result_idx
                ))?
                .start_line
                .ok_or(format!(
                    "SARIF missing field `runs[{}].results[{}].locations[0].physicalLocation.region.startLine`",
                    run_idx, result_idx
                ))?;

            let location = codequality::Location {
                path: file_path,
                line: file_lineno,
            };

            let cq_issue = codequality::Issue::new(check_name, description, severity, location);

            code_climate_data.push(cq_issue);
        }
    }

    info!("Conversion done");
    info!("Found {} issues", code_climate_data.len());

    info!("Serializing...");
    let json_out_str = serde_json::to_string(&code_climate_data)?;

    let str_len_kilobytes: f32 = json_out_str.chars().count() as f32 / 1024.0;
    if str_len_kilobytes > (8.0 * 1024.0) {
        warn!(
            "This is a large file ({:.3} MBytes)! GitLab may not show this report in Merge Requests.",
            str_len_kilobytes / 1024.0
        )
    }

    info!("Saving output to {}", args.output_file);
    fs::write(args.output_file, json_out_str)?;

    Ok(())
}

/// Convert Sarif "result" level into a Code Quality severity enum.
///
/// SARIF result level is in `{"runs": ["results": [{"level": "<level_str>", ...}]]}`
fn from_sarif_result(
    result_level: serde_json::Value,
) -> Result<codequality::Severity, Box<dyn Error>> {
    // SARIF result kinds: "notApplicable", "pass", "fail", "review", "open", "informational"
    // SARIF result levels: "none", "note", "warning", "error"

    if result_level == "error" {
        return Ok(codequality::Severity::Major);
    } else if result_level == "warning" {
        return Ok(codequality::Severity::Minor);
    } else if result_level == "note" {
        return Ok(codequality::Severity::Info);
    }

    Err(format!(
        "Could not convert SARIF result.level \"{}\" into a Code Quality severity!",
        result_level
    )
    .into())
}
